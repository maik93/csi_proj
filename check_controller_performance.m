%% check_controller_performance: function that checks NS, NP, RS and RP for a given generalized plant P (8x8) controlled by K (2x2)

function [] = check_controller_performance(name,P,K,omega)

    % everything here must be plotted in absolute values, not in dB
    bode_opts = bodeoptions; bode_opts.MagUnits  = 'abs';

    % N-Delta structure (N is 6x6, [y_delta, z]' = N * [u_delta, w]')
    %    N is computed as: P11 + P12 * K / (eye(2) - P22 * K) * P21
    N = lft(P,K); % same as above but faster

    fprintf('Checking%s...\n', name);
    if isstable(N) == 1 % Nominal Stability Check
        fprintf(' -> N is internally stable. Nominal Stability satisfied.\n');
    
        % ---------------------- Nominal Performance Check ----------------------

        % first method: manual computation of inf-norm of N22
        % N22 = N(3:6,3:6);
        % sigma_N22 = zeros(length(omega),1);
        % for i=1:length(omega)
        %     % evaluate induced 2-norm (= maximum singular value)
        %     sigma_N22(i) = norm(frd(N22, omega(i)).ResponseData,2);
        %     % max(svd(frd(N22, omega(i)).ResponseData)) % same result as above
        % end
        % figure, semilogx(omega,sigma_N22), grid

        % second method: using mussv, compute mu with the unstructured Delta_P (full complex matrix)
        N22 = frd(N(3:6,3:6),omega);
        Delta_P_structure = [4,4]; % means one complex 4-dimensional block for Delta_P
        muNP_bounds = mussv(N22, Delta_P_structure, 'Us');
        muNP = muNP_bounds(:,1);

        % figure, bodemag(muNP,bode_opts), grid
        % title('Nominal Performance (Maximum Singular Value of N22)')

        fprintf(' -> Maximum peak of Nominal Performance: %f\n',norm(muNP,inf));

        % ----------------------- Robust Stability Check -----------------------
        N11 = frd(N(1:2,1:2),omega);
        Delta_structure = [1,1;1,1]; % means that Delta is a 2x2 diagonal complex matrix
        muRS_bounds = mussv(N11, Delta_structure, 'Us');
        muRS = muRS_bounds(:,1);

        % figure, bodemag(muRS,bode_opts), grid
        % title('Robust Stability (mu_Delta of N11)')

        fprintf(' -> Maximum peak of Robust Stability:    %f\n',norm(muRS,inf));

        % ----------------------- Robust Performance Check -----------------------
        N_frd = frd(N,omega);
        Delta_hat_structure = [Delta_structure; Delta_P_structure]; % Delta_hat is block diagonal matrix with Delta and Delta_P
        muRP_bounds = mussv(N_frd, Delta_hat_structure, 'Us');
        muRP = muRP_bounds(:,1);

        % figure, bodemag(muRP,bode_opts), grid
        % title('Robust Stability (mu_Delta_hat of N)')

        fprintf(' -> Maximum peak of Robust Performance:  %f\n',norm(muRP,inf));

        % ----------------------- Plotting all togheter -----------------------
        figure, bodemag(muNP,'r',muRS,'b',muRP,'k',bode_opts), grid
        title(strcat('Mu analysis of ',name))
        xlabel('Frequency (rad/min)')
        legend('Nominal Performance','Robust Stability','Robust Performance')

    else
        fprintf(' -> N is NOT internally stable!! Nominal Stability is NOT satisfied!\nThe analysis of this controller will not go any further.');
    end
end
