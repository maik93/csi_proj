% Approximation of uncertain gain and time delay using multiplicative input perturbations on each input

figure, hold on, grid on
om = logspace(-2,2,150);

c=0;
for k = 0.8:0.1:1.2
    for tau = 0:0.05:1.0
        pert = zeros(length(om),1);
        for i = 1:length(om)
            pert(i) = sqrt((k*cos(om(i)*tau)-1)^2 + (k*sin(om(i)*tau))^2);
        end
        pert_frd = frd(pert,om);
        bodemag(pert_frd,'c--')
        fprintf('%d scimmie saltavano sul letto... una cadde in terra e si ruppe il cervelletto\n', 99-c), c=c+1;
    end
end

% first-order boundary
theta_max = 1; % [rad/min] obv theta_min is 0
k_min = .8;
k_max = 1.2;
% k_avg = (k_min + k_max) / 2; % TODO: formally it should multiply G0? (p.272)
r_k = (k_max - k_min) / (k_min + k_max);
w1 = tf([(1+r_k/2)*theta_max r_k],[theta_max/2 1]);
bodemag(frd(w1,om),'r.-')

% correction factor around frequencies 1/theta_max
w2 = tf([(theta_max/2.363)^2, 2*.838*theta_max/2.363, 1], ...
      [(theta_max/2.363)^2, 2*.685*theta_max/2.363, 1]);
% bodemag(frd(w2,om),'g.')

% final boundary
wI = w1*w2;
bodemag(frd(wI,om),'r')

% boundary choosen by the book
% W_Delta = tf([2.2138 15.9537 27.6702  4.9050], ...
%              [1.      8.3412 21.2393 22.6705]);
% W_Delta_frd = frd(W_Delta,om);
% %
% bodemag(W_Delta_frd,'r.-')

xlabel('Frequency (rad/min)')
ylabel('Magnitude')
title('Approximation of uncertain gain and time delay by multiplicative perturbation')

clear om c k tau pert om pert_frd theta_max k_min k_max r_k w1 w2
