% this is the resulting weight from compute_unc_input, corresponding to a diagonal Delta 2x2
wI = tf([0.196999553527376,0.816012768436517,1.24185357596276,0.2], ...
		[0.0895452516033525,0.468976241674754,1.07977147693610,1]);

% performance weights on outputs y
wy_i = .55 * tf([9.5 3],[9.5 1e-4]);
Wy = [wy_i .3; .3 wy_i];

% performance weights on controls u
wu_i = .87 * tf([1 1],[.01 1]);
Wu = blkdiag(wu_i, wu_i);

% final performance weight matrix 4x4, over the vector z = [e_y, u]'
WP = blkdiag(Wy, Wu);

% noise shaping filter on y measurements
wn_i = 1e-2 * tf([1 0],[1 1]);
Wn = blkdiag(wn_i, wn_i);

if plot_everything
	figure, bodemag(1/wy_i,omega), grid
	title('Inverse of Performance Weighting Function')

	figure, bodemag(wu_i,omega), grid
	title('Control Action Weighting Function')

	figure, bodemag(wn_i,omega), grid
	title('Sensor Noise Weight')
end

clear wy_i wu_i wn_i
