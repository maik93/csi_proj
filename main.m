%% Common things to define used in almost all sections below

Simulink.fileGenControl('set', 'CacheFolder', 'simulink_cache');

plot_everything = true;

% Range of interest for all bode plots
omega = logspace(-4,2,100); % from 0.0001 to 100 rad/min

% the system should settle at [.995 .005]
step_requested = [.995*tf(1,[1 0]); .005*tf(1,[1 0])];

% Open-loop transfer function for the Distillation Column system
get_linear_model
% -> G4:  is the scaled model with 82 states and 4 inputs
% -> G82: is the scaled model with 82 states and 2 inputs
% -> G:   is the scaled model with  6 states and 2 inputs

% Desired closed loop transfer function from r_i to y_i
T = 6; xi = 0.8;
des_cl_i = tf(1,[T 2*xi*T 1]); % settling time: ~35 seconds
des_cl = [des_cl_i 0; 0 des_cl_i]; % diagonal tf matrix

clear T xi des_cl_i

%% Plot singular values of G and G_4
if plot_everything
    figure, sigma(G,'r-',G82,'b-.',omega), grid on
    title('Singular Value Plots of G and G_4')
    xlabel('Frequency (rad/min)'), ylabel('Magnitude')
    legend('Singular values of G','Singular values of G4')
end

%% Approximation of uncertain gain and time delay using
%   multiplicative input perturbations on each input

% this script plots the set of allowed uncertainties and defines a weight
%   wI that can contain them (it's commented because slow to compute):
% compute_unc_input

% load just that resulting weight and others (performances and noises) with this script
load_weights

%% Generalized plant

% perturbances: diagonal matrix 2x2
Delta_1 = ultidyn('Delta_1',[1 1]);
Delta_2 = ultidyn('Delta_2',[1 1]);
Delta = blkdiag(Delta_1,Delta_2);

% Generalized transfer function matrix P for 1dof control:
% [y_delta, e_y, e_u, v]' = P * [u_delta, r, n, u]'
%    where each of those variables is 2-dimensional
P_1dof = minreal([zeros(2),   zeros(2), zeros(2), wI*eye(2); ...
                      Wy*G, -Wy*des_cl, zeros(2),      Wy*G; ...
                  zeros(2),   zeros(2), zeros(2),        Wu; ...
                        -G,     eye(2),      -Wn,        -G], [], false);

% Generalized transfer function matrix P for 2dof control:
% [y_delta, e_y, e_u, r, y_m]' = P * [u_delta, r, n, u]'
%    where each of those variables is 2-dimensional
P_2dof = minreal([zeros(2),   zeros(2), zeros(2), wI*eye(2); ...
                      Wy*G, -Wy*des_cl, zeros(2),      Wy*G; ...
                  zeros(2),   zeros(2), zeros(2),        Wu; ...
                  zeros(2),     eye(2), zeros(2),  zeros(2); ...
                         G,   zeros(2),       Wn,         G], [], false);

clear Delta_1 Delta_2

%% RGA analysis. SVD, PIDs and DNA controllers
classical_controllers

%% LQG servo (with integral action)
Q   = .01 * eye(6); % state weights
R   =   1 * eye(2); % control weights
Qxu = blkdiag(Q,R); % block diagonal matrix for lqg function
Qi  = .01 * eye(2); % integrative weights
% noise and disturbances covariances
Qwv = norm(Wn,inf) * eye(8); % W and V as maximum peak of n's covariance
K_lqg_1dof = lqg(G,Qxu,Qwv,Qi,'1dof');

% little correction that improve 2DoF performances
Qwv = norm(Wn,inf) * blkdiag(10*eye(6), 1*eye(2));
K_lqg_2dof = lqg(G,Qxu,Qwv,Qi,'2dof');

clear Q R Qxu Qwv Qi

check_controller_performance(' 1dof LQG servo', P_1dof, K_lqg_1dof, omega)
check_controller_performance(' 2dof LQG servo', P_2dof, K_lqg_2dof, omega)

%% LTR on output (Kalman filter tf recovery)

% % S influence over C*inv(sI-A)*Kf (Kalman filter transfer function)
% V = eye(2);
% figure, hold, c=1;
% for i=0:0.2:10
%     S = i * eye(2);
%     W = (B*S)*(B*S)';
%     Kf = lqr(A',C',W,V); Kf = Kf';
%     sigma(A,Kf,C,zeros(2,2),omega);
%     c=c+1;
% end

S = 10 * eye(2); % scaling matrix that alter eigenvalues of W
W = (B*S)*(B*S)'; % = Xi in ltrsyn documentation
V = eye(2); % = Th in ltrsyn documentation
Kf = lqr(A',C',W,V); Kf = Kf'; % solve Kalman filter as dual problem of LQR
sigma(A,Kf,C,zeros(2,2),omega); % plot singular values of C*inv(sI-A)*Kf

Q = C' * C; R = eye(2); % initial control weights
rho = [1, 1e2, 1e4, 1e5]; % recovery gains for R, K_ltr is that one with the last of them
K_ltr_y = ltrsyn(G, Kf, Q, R, rho, omega, 'OUTPUT'); % LQG/LTR at y (plant out)

clear S W V Kf Q R rho

check_controller_performance(' LQG/LTR on y', P_1dof, K_ltr_y, omega)

%% LTR on input (LQR tf recovery)

S = 10 * eye(2); % scaling matrix that alter eigenvalues of W
Q = (S*C)' * (S*C); % = Xi in ltrsyn documentation
R = eye(2); % = Th in ltrsyn documentation
Kr = lqr(A,B,Q,R);
sigma(A,B,Kr,zeros(2,2),omega); % plot singular values of Kr*inv(sI-A)*B

W = B * B'; V = eye(2); % initial control weights
rho = [1, 1e2, 1e4, 1e5]; % recovery gains for R, K_ltr is that one with the last of them
K_ltr_u = ltrsyn(G, Kr, W, V, rho, omega); % LQG/LTR at u (plant in)

clear S W V Kr Q R rho

check_controller_performance(' LQG/LTR on u', P_1dof, K_ltr_u, omega)

% LQ with integral action
% R = eye(2);
% Q = blkdiag(zeros(6),eye(2));
% % expand A and B with integral of (r-y)
% A_int = [A, zeros(6,2); -C, zeros(2,2)];
% B_int = [B;-D];
% Kr = lqr(A_int,B_int,Q,R);

% TODO: try with lqi instead of lqr for input recovery
% https://it.mathworks.com/help/control/ref/lqi.html

%% H-inf synthesis through loopshaping

% pre- and post-compensators W1 and W2
w1 = 1.7 * tf([1.1,1],[10,0]);
W1 = blkdiag(w1,w1);
W2 = eye(2);

% desired transfer function: W2*G*W1
Gs = W2*G*W1;

if plot_everything
    sigma(G,'b-',Gs,'r--',omega), grid
    title('Frequency responses of the original plant and shaped plant')
    xlabel('Frequency (rad/min)'), ylabel('Magnitude')
    legend('Original plant','Shaped plant')
end

K_Hinf = ncfsyn(-G,W1,W2);

clear  w1 W1 W2 Gs

check_controller_performance(' H-inf controller', P_1dof, K_Hinf, omega)

%% Mu synthesis using DK-iteration

% perturbed generalized plant (incorporate Delta in the P form)
P_pert_1dof = lft(Delta,P_1dof);
P_pert_2dof = lft(Delta,P_2dof);

% compute Mu control over the usual frequency range
mu_opts = dksynOptions('FrequencyVector',omega);
K_mu_1dof = dksyn(P_pert_1dof, 2, 2, mu_opts); % 32-th order
K_mu_2dof = dksyn(P_pert_2dof, 4, 2, mu_opts); % 34-th order
% mumbers are referred to I/O for the controller, v and u, numbered from the last element of P

clear P_pert_1dof P_pert_2dof mu_opts

check_controller_performance(' 1dof Mu controller', P_1dof, K_mu_1dof, omega)
check_controller_performance(' 2dof Mu controller', P_2dof, K_mu_2dof, omega)

%% Mu controller reduction for 1DoF

K_mu_1dof_reduced = reduce(K_mu_1dof, 6);
if plot_everything
    sigma(K_mu_1dof,'b-',K_mu_1dof_reduced,'r--',omega), grid
    title('Frequency responses of the original and reduced 1dof Mu controller')
    xlabel('Frequency (rad/min)'), ylabel('Magnitude')
    legend('Original controller','reduced controller')
end
check_controller_performance(' 1dof Mu reduced controller', P_1dof, K_mu_1dof_reduced, omega)
% maximum peaks are almost as the original controller

%% Mu controller reduction for 2DoF

K_mu_2dof_reduced = reduce(K_mu_2dof, 13);
if plot_everything
    sigma(K_mu_2dof,'b-',K_mu_2dof_reduced,'r--',omega), grid
    title('Frequency responses of the original and reduced 2dof Mu controller')
    xlabel('Frequency (rad/min)'), ylabel('Magnitude')
    legend('Original controller','reduced controller')
end
check_controller_performance(' 2dof Mu reduced controller', P_2dof, K_mu_2dof_reduced, omega)
% NP and RS are increased by a quarter, while RP is no more satisfied
% a reduction to 13-th order is the limit for Robust Performance (=0.957)