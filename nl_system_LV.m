% Original file: cola_lv.m

% NL system with 82 states, where we use D to control M_D and B to control M_B (done by two proportional controllers with both gains equal to 10). This is done in order to stabilize the plant and get the free evolution.

function xprime = nl_system_LV(t,X) 
% sample usage:   [t,x]=ode15s(@cola_lv,[0 5000],0.5*ones(1,82));
%
% cola_lv - Subroutine for simulation with LV-configuration.
%           It calls the model colamod, and 
%           includes control of condenser and reboiler level 
%           using two P-controllers with the LV-configuration. 
%
%           Inputs are reflux (LT) and boilup (VB) for each stage, so 2*NT.
%           Disturbances are feedrate F and feed composition xF.
%
%           Outputs are liquid composition and liquid hold up time derivatives
%           for stages 1 through NT, with same dimemsions as X.
%
% Number of stages in the column
NT = 41;
%
% Inputs and disturbances
LT = 2.70629;                % Reflux
VB = 3.20629;                % Boilup
F = 1.0;                     % Feedrate
zF = 0.5;                    % Feed composition
qF = 1.0;                    % Feed liquid fraction
%
% P-Controllers for control of reboiler and condenser hold up
KcB = 10;  KcD = 10;         % controller gains
MDs = 0.5; MBs = 0.5;        % Nominal holdups - these are rather small  
Ds = 0.5; Bs = 0.5;          % Nominal flows
MB = X(NT+1);  MD = X(2*NT); % Actual reboiler and condenser holdup
D = Ds + (MD-MDs)*KcD;       % Distillate flow
B = Bs + (MB-MBs)*KcB;       % Bottoms flow     
%
% Store all inputs and disturbances
U(1) = LT; U(2) = VB; U(3) = D; U(4) = B; 
U(5) = F;  U(6) = zF; U(7) = qF;
%
xprime = nl_system(t,X,U);