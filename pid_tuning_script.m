% Find a PID diagonal control suitable for G through tuning of 'pid_tuning.slx'
%   The result is tested in 'pid_tuned.slx', where saturations are implemented.
% from here: https://it.mathworks.com/help/slcontrol/ug/looptune.html

mdl = 'pid_tuning';
open_system(mdl);

% switch over u2=0
set_param('pid_tuning/sw', 'sw', '1')

% slTuner interface for the model
st_u1 = slTuner(mdl,'PID_u1');

% Add the PID Controller output u and measure y as analysis points to st_u1
addPoint(st_u1,'u1');
addPoint(st_u1,'r');
addPoint(st_u1,'y');

% target crossover frequency: 0.5 rad/min
wc = .5;
st = looptune(st_u1,'u1','y',wc);

% plots
% stepplot(getIOTransfer(st_u1,'r','y'),getIOTransfer(st,'r','y'));
% legend('Initial','tuned');

showTunable(st)
writeBlockValue(st)

% save_system

% switch over PID_u2
set_param('pid_tuning/sw', 'sw', '0')

st_u2 = slTuner(mdl,'PID_u2');

addPoint(st_u2,'u2');
addPoint(st_u2,'r');
addPoint(st_u2,'y');

st = looptune(st_u2,'u2','y',wc);

showTunable(st)
writeBlockValue(st)

% save_system

% clear mdl st_u1 wc st st_u2
