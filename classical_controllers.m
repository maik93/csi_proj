%% Controllability of each input
% R1 = ctrb(A,B(:,1));
% rank(R1)
% R2 = ctrb(A,B(:,2));
% rank(R2)
% % both has full rank, so any input could control both outputs

%% RGA at steady state
% The basic rules are:
% 1. Avoid input and output pairs which have negative relative gains.
% 2. Avoid input and output pairs which have large relative gains.
% 3. Select input and output pairs which have the relative gain close to 1.
G_steady = frd(G,0).ResponseData;
rga = inv(G_steady.').*G_steady %#ok<NASGU,NOPTS>
% we see here that each input is paired with both outputs

% Iterative RGA can suggest which I/O pairs choose, resulting as a permutation matrix
iterative_rga = G_steady;
for i=1:8 % 8 is the minimum iteration number that makes det(iRGA) almost 1
    iterative_rga = inv(iterative_rga.').*iterative_rga;
end
iterative_rga %#ok<NOPTS>
% and this confirms that we should choose u1->y1, u2->y2

% let's now look not only at steady state, but in all working frequencies
% if plot_everything
%     rga_rfd = frd(G,omega);
%     G_w = rga_rfd.ResponseData;
%     rga_w = zeros(2,2,length(G_w));
%     for i=1:length(G_w)
%         rga_w(:,:,i) = inv(G_w(:,:,i).').*G_w(:,:,i);
%     end
%     rga_rfd.ResponseData = rga_w;
%     figure, bodemag(rga_rfd), grid on
% end

clear G_steady rga iterative_rga rga_rfd G_w rga_w

%% SVD controller
w0 = 0; % [rad/min] where to evaluate the decomposition
G_0_cmplx = frd(G,w0); G_0_cmplx = G_0_cmplx.ResponseData; % can be a complex matrix
G_0 = real(G_0_cmplx);
[U,S,V] = svd(G_0);
% conditioning number: S(1,1)/S(2,2) -> 148.1848 -> strong directionality
K_diag = des_cl / S;
K_svd = V * K_diag * U';

% add integral action
in_ui = tf([1 .01],[1 0]);
integrator = blkdiag(in_ui,in_ui);
K_svd_int = minreal(V * integrator * K_diag * U');

clear w0 G_0_cmplx G_0 U S V K_diag in_ui

check_controller_performance(' SVD controller', P_1dof, K_svd, omega)
check_controller_performance(' SVD with integrator controller', P_1dof, K_svd_int, omega)

%% PID tuning
% a diagonal control with PIDs is tuned with simulink by using this script:
% pid_tuning_script

% that script produces this controller
p = 0.989781579533969;   i = 0.0560263286357952;
d = -0.0154153213803847; n = 104.80757205168;
K_1 = tf([p+d*n, n*p+i, n*i],[1, n, 0]);
p = -0.279233712240472;   i = -0.156083882651768;
d = -0.00225333098333346; n = 110.923188165224;
K_2 = tf([p+d*n, n*p+i, n*i],[1, n, 0]);
K_pid = blkdiag(K_1,K_2);

clear p i d n K_1 K_2

check_controller_performance(' diagonal PID controller', P_1dof, K_pid, omega)

%% DNA (Direct Nyquist Array) diagonalizing with adj(G)
Gtf = tf(G);
Diagonalizer = [Gtf(2,2) -Gtf(1,2); -Gtf(2,1) Gtf(1,1)]; % adj(G)
Diagonalized_G = minreal(Gtf * Diagonalizer); %#ok<NASGU>
% sisotool(Diagonalized_G(1,1)) % a PI control named K_pi_Ui is then generated
K_pi_Ui = tf([-0.07918 -0.000612],[1 0]); % resulting from sisotool
K_dna_diag = blkdiag(K_pi_Ui,K_pi_Ui);
K_dna = Diagonalizer * K_dna_diag;

clear Gtf Diagonalizer Diagonalized_G K_pi_Ui K_dna_diag

check_controller_performance(' Direct Nyquist Array controller', P_1dof, K_dna, omega)
