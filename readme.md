# Control of a Distillation Column with Classical and Advanced Methods

![distillation column](https://gitlab.com/maik93/csi_proj/raw/master/report/img/dist_column_schema.png)

For *Control of Uncertain Systems* course a MIMO highly coupled system, the **Distillation Column**, is studied and controlled in order to achieve requested performances. The entire project is developed in Matlab and Simulink.

Delivery date: March 1, 2019

## Full documentation
A comprehensive documentation of this work can be found on the [uploaded report](https://gitlab.com/maik93/csi_proj/blob/master/report/report.pdf), covering dynamic model description, classical controls realization, LQG, Hinf and Mu controllers seen both in theory than in practice.

## Presentation slides
Slides used in support of the exposition during the exam can be found [here](https://gitlab.com/maik93/csi_proj/raw/master/report/CSI_Michael_Mugnai.pptx).